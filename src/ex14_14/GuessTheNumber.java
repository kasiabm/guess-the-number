package ex14_14;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;

/**
 * Simple class playing "Guess the number" game.
 * @author KasiaBM
 * @version Jan 9, 2015
 */

public class GuessTheNumber extends JFrame {
    //instance variables
    private Random rand;
    private int guess;
    private int lastGuess;
    private int number;
    private final String[] labels = {"<html>I have a number between 1 and 1000."
            + "<br>Can you guess my number?<br>Please enter your first guess.</html>",
            "Too high", "Too low", "Correct!", "Guess", "New Game"};
    private JLabel instructionLabel;
    private JTextField guessField;
    private JButton guessButton;
    private JButton newGameButton;
    private Color panelColour;
    
    //constructor
    public GuessTheNumber() {
        //sets up the frame
        super("Guess The Number");
        setLayout(new FlowLayout());
        setSize(270, 150);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //sets variables
        rand = new Random();
        guess = 0;
        lastGuess = 0;
        number = 1 + rand.nextInt(1000);
        instructionLabel = new JLabel(labels[0]);
        guessField = new JTextField(20);
        guessButton = new JButton(labels[4]);
        newGameButton = new JButton(labels[5]);
        panelColour = getBackground();
        
        //adds components to the containers
        add(instructionLabel);
        add(guessField);
        add(guessButton);
        add(newGameButton);
        
        //registers event handlers
        guessButton.addActionListener(new GuessButtonHandler());
        newGameButton.addActionListener(new NewGameButtonHandler());
                
        //sets window to visible
        setVisible(true);
    } //end constructor GuessTheNumber
    
    //event handlers
    //private class handling guessField events
    private class GuessButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String input = guessField.getText();
            lastGuess = guess;
            if (input.isEmpty()) {
                instructionLabel.setText("<html><br>Enter your guess between"
                        + " 1 and 1000.<br><br></html>");
                instructionLabel.setForeground(Color.BLACK);
            }
            else {
                try { 
                    guess = Integer.parseInt(input);
                    if (guess < 1 || guess > 1000) {
                        lastGuess = 0;
                        instructionLabel.setText("<html><br>Enter your guess "
                                + "between 1 and 1000.<br><br></html>");
                        guessField.setText("");
                        instructionLabel.setForeground(Color.BLACK);
                    }
                    else {
                        if (Math.abs(number - guess) > lastGuess) {
                            getContentPane().setBackground(Color.BLUE);
                            instructionLabel.setForeground(Color.WHITE);
                        }
                        if (Math.abs(number - guess) < lastGuess) {
                            getContentPane().setBackground(Color.RED);
                            instructionLabel.setForeground(Color.WHITE);
                        }
                        if (guess > number) {
                            instructionLabel.setText("<html><br>" + labels[1]
                                    + "<br><br></html>");
                        }
                        if (guess < number) {
                            instructionLabel.setText("<html><br>" + labels[2]
                                    + "<br><br></html>");
                        }
                        if (guess == number) {
                            getContentPane().setBackground(Color.GREEN);
                            instructionLabel.setText("<html><br>" + labels[3]
                                    + "<br><br></html>");
                            guessField.setEditable(false);
                            instructionLabel.setForeground(Color.BLACK);
                        }
                    }
                }
                catch (NumberFormatException ex) {
                    instructionLabel.setText("<html><br>Enter your guess between"
                        + " 1 and 1000.<br><br></html>");
                    guessField.setText("");
                }
            }
        } //end method actionPerformed
    } //end class GuessFieldHandler
    
    //private class handling newGameButton events
    private class NewGameButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            guess = 0;
            lastGuess = 0;
            number = 1 + rand.nextInt(1000);
            instructionLabel.setText(labels[0]);
            guessField.setText("");
            guessField.setEditable(true);
            getContentPane().setBackground(panelColour);
            instructionLabel.setForeground(Color.BLACK);
        } //end method actionPerformed
    } //end class NewGameButtonHandler
} //end class GuessTheNumber